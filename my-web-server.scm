;;; Copyright © 2021 Hugo Lecomte <hugo.lecomte@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu packages)
             (guix git)
             ((guix channels) #:select (%default-channels))
             ((guix build utils) #:select (mkdir-p))
             (ice-9 getopt-long)
             (ice-9 match)
             (json)
             (rnrs bytevectors)
             (rnrs io ports)
             (srfi srfi-1)
             (srfi srfi-19)
             (sxml simple)
             (web request)
             (web response)
             (web server)
             (web uri))

(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))

(define (not-found request)
  (values (build-response #:code 404)
          (string-append "Ressource not found: "
                         (uri->string (request-uri request)))))

(define favicon
  (call-with-input-file "static/images/favicon.png" get-bytevector-all))

(define results
  (call-with-input-file "static/results.JSON" get-string-all))

(define (html-page)
  `((html (@ (lang "en"))
          (head
           (title "Benchmark")
           (meta (@ (http-equiv "Content-Type") (content "text/html; chartset=utf-8")))
           (link (@ (rel "icon")
                    (type "image/x-icon")
                    (href "static/images/favicon.png")))
           (script (@ (type "text/javascript")
                      (src "https://cdn.jsdelivr.net/npm/chart.js@3.4.1/dist/chart.min.js")) "")
           (script (@ (type "text/javascript")
                      (src "https://cdn.jsdelivr.net/npm/hammerjs@2.0.8/hammer.min.js")) "")
           (script (@ (type "text/javascript")
                      (src "https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@1.1.1/dist/chartjs-plugin-zoom.min.js")) "")
           
           )
          (body
           (canvas (@ (id "myChart")) "")
           (script (@ (type "text/javascript") (src "main.js")) "")
           (button (@ (onclick "resetZoomChart()")) "Reset Zoom")))))

(define (main-page-handler request body)
  (values '((content-type . (text/html)))
          (call-with-output-string
            (lambda (port)
              (format port "<!DOCTYPE html>~%")
              (sxml->xml (html-page) port)))))

(define (favicon-handler request body)
  (values '((content-type . (image/x-icon)))
          (call-with-input-file "static/images/favicon.png" get-bytevector-all)))

(define (main-js-handler request body)
  (values '((content-type . (text/javascript)))
          (call-with-input-file "main.js" get-string-all)))

(define (results-handler request body)
  (values '((content-type . (application/javascript)))
          (call-with-input-file "static/results.JSON" get-string-all)))

(define (request-handler request body)
  (define request-path
    (uri-path (request-uri request)))

  (match (request-method request)
    ('GET
     (cond ((string= request-path "/static/images/favicon.png")
            (favicon-handler request body))
           ((string= request-path "/main.js")
            (main-js-handler request body))
           ((string= request-path "/results.JSON")
            (results-handler request body))
           ((string= request-path "/")
            (main-page-handler request body))))
    (_
     (not-found request)))
  )

(run-server request-handler)
