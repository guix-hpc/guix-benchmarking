;;; Copyright © 2021 Hugo Lecomte <hugo.lecomte@inria.fr>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (srfi srfi-19))
(use-modules (srfi srfi-9))
(use-modules (ice-9 match))
(use-modules (guix build utils))
(use-modules (json))
(use-modules (srfi srfi-69))
(use-modules (srfi srfi-1))
(use-modules (ice-9 popen))
(use-modules (ice-9 rdelim))
(use-modules (guix))
(use-modules (guix channels))
(use-modules (guix inferior))

(define-record-type <benchable-command>
  ;;This type contain everything you need to benchmark a Guix command:
  ;; -the command itself
  ;; -a procedure that do whatever we want but must have a directory from which we run Guix
  ;;  and a command to run as parameters. It must return an association list with one or
  ;;  more pairs composed of string and any value.
  (make-benchable-command command metrics)
  benchable-command?
  (command benchable-command-command)
  (metrics benchable-command-metrics))

;;Here are some benchmarking procedures
(define (benchmark-current-time directory command)
  (define invoke-args
    (append (list (string-append directory "/bin/guix")) (string-split command #\ )))
  (define start (time-nanosecond (current-time time-process)))
  (parameterize ((current-output-port (%make-void-port "w")))
    (apply invoke invoke-args))
  (list `( "time" . ,(vector (- (time-nanosecond (current-time time-process)) start)))))

(define (benchmark-time directory command)
  (define shell-command
    (string-append "\\time -p " directory "/bin/guix " command " 2>&1 > /dev/null"))
  (let* ((port (open-input-pipe shell-command))
         (str (read-string port)))
    (close-pipe port)
    (match (string-tokenize str)
      (("real" real-value "user" user-value "sys" sys-value)
       `(("real" . ,(string->number real-value))
         ("user" . ,(string->number user-value))
         ("sys" . ,(string->number sys-value)))))))

(define (benchmark-package-graph-complexity directory command)
  (define shell-command
    (string-append directory "/bin/guix " command " | grep 'label =' | wc -l"))
  (let* ((port (open-input-pipe shell-command))
         (str (read-line port)))
    (close-pipe port)
    (list `(" " . ,(string->number str)))))

(define (benchmark-strace directory command)
  (define shell-command
    (string-append "strace -c " directory "/bin/guix " command " 2>&1 > /dev/null | grep total"))
  (let* ((port (open-input-pipe shell-command))
         (str (read-string port)))
    (close-pipe port)
    (match (string-tokenize str)
      (("100.00" seconds usecs/call calls errors "total")
       `(("seconds" . ,(string->number seconds))
         ("usecs/call" . ,(string->number usecs/call))
         ("calls" . ,(string->number calls))
         ("errors" . ,(string->number errors)))))))

;;Procedure used to format benchmarking procedure's results   
(define (assoc-set k v alist)
  (alist-cons k v (alist-delete k alist)))

(define (assoc-append-and-set alist pair)
  (match pair
    ((k . v)
     (match (assoc k alist)
       ((_ . value-list) (assoc-set k (append value-list (list v)) alist))
       (#f (assoc-set k (list v) alist))))))

(define (benchmark-metric guix-directories metric command)
  (fold
   (lambda (item result)
     (fold
      (lambda (sub-item sub-result)
        (assoc-append-and-set sub-result sub-item))
      result
      item))
   `()
   (map (lambda (guix-directory)
          (metric guix-directory command)) guix-directories)))

(define (process-benchmarked-metric guix-directories metric command)
  `(,(procedure-name metric) .
    ,(map (lambda (item)
            (match item
              ((k . v) `(,k . ,(list->vector v)))))
          (benchmark-metric guix-directories metric command))))

(define (benchmark-one-guix-command guix-directories guix-command)
  `(,(benchable-command-command guix-command) .
    ,(map (lambda (metric)
           (process-benchmarked-metric guix-directories metric (benchable-command-command guix-command)))
         (benchable-command-metrics guix-command))))

(define (commit->channel commit)
  (channel
   (name 'guix)
   (url "https://git.savannah.gnu.org/git/guix.git")
   (commit
    commit)
   (introduction
    (make-channel-introduction
     "9edb3f66fd807b096b48283debdcddccfea34bad"
     (openpgp-fingerprint
      "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

(define (benchmark-guix-commands commit-list guix-commands)
  (define s (open-connection))
  (define guix-directories
    (map (lambda (channel)
           (cached-channel-instance s (list channel)))
         (map commit->channel commit-list)))
  (map (lambda (guix-command)
         (benchmark-one-guix-command guix-directories guix-command))
       guix-commands))


(define guix-commands
  (list
   (make-benchable-command "help"
                           (list benchmark-current-time benchmark-time benchmark-strace))
   (make-benchable-command "install --help"
                           (list benchmark-current-time benchmark-strace))
   (make-benchable-command "search game > /dev/null"
                           (list benchmark-current-time))
   (make-benchable-command "build libreoffice --no-grafts -d"
                           (list benchmark-current-time))
   (make-benchable-command "graph -t bag coreutils"
                           (list benchmark-package-graph-complexity))
   (make-benchable-command "graph -t derivation coreutils"
                           (list benchmark-package-graph-complexity))
   (make-benchable-command "graph guix"
                           (list benchmark-package-graph-complexity))))

(define commit-list
  (list
   "b48bc8953db6b9bd7704c2a9c78f16f502ec6e22"
   "4e19ad0fde26266a5e3a9ec6d73ad3bf88fc189d"
   "785c96fbe162c55c46b7a14bbfe7ccade8f063f6"
   "89ea0918a4a6cc9c250b85c0b713e471b7769c48"
   "d98fc80ffbda32e184f214f02b4d9b2d72c9c90a"
   "aa2686c368d66e3b10d9f4c6e462a271d5911d70"
   "9cfa6c3c587766a310252f4c1271503d819bdd8a"))

;;Create a JSON file, copy commit-list in it, benchmark guix-command for commit-list and copy results in JSON file
(call-with-output-file "static/results.JSON"
  (lambda (port)
    (scm->json `(("commits" . ,(list->vector commit-list))
                 ("benchmark" . ,(benchmark-guix-commands commit-list guix-commands))) port #:pretty #t)))






