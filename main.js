// Copyright © 2021 Hugo Lecomte <hugo.lecomte@inria.fr>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//This part correspond to the graph declaration and initialization of it's variables
var cv = document.getElementById('myChart');
var ctx = cv.getContext('2d');
const data =  {
    labels: [

    ],
    datasets: [
              ]
};

const scaleOpts = {
    grid: {
        color: 'rgba( 0, 0, 0, 0.1)',
    },
};
const scales = {
    x: {
        type: 'category',
        title: {
            display: true,
            text: 'Commit',
        },
        ticks: {
            callback: function(value) {
                //limit label size displayed on graph to ten characters 
                return this.getLabelForValue(value).substr(0, 10);
            }
        }
    },
    y: {
        type: 'linear',
        title: {
            display: true,
            text: 'Performances',
        },
        ticks: {
            callback: (val, index, ticks) => index === 0 || index === ticks.length - 1 ? null : val,
        },
    },
};
Object.keys(scales).forEach(scale => Object.assign(scales[scale], scaleOpts));

const config = {
    type: 'line',
    data: data,
    options: {
        'onClick' :function(evt){
            //on click on any point of the chart, open new tab in browser on associated commit
            var activePoints = this.getElementsAtEventForMode(evt, 'nearest', { intersect: true }, true);
            if(activePoints.length){
                console.log('activePoints', activePoints);
                var label = this.data.labels[activePoints[0].index];
                window.open("https://git.savannah.gnu.org/cgit/guix.git/commit/?id=" + label);
            }
        },
        scales: scales,
        plugins: {
            title: {
                display: true,
                text: 'Guix Help benchmark',

            },
            zoom: {
                pan: {
                    enabled: true,
                    mode: 'x',
                    modifierKey: 'ctrl',
                },
                zoom: {
                    drag: {
                        enabled: true
                    },
                    mode: 'x',
                },
            }
        },
    }
};

var myChart = new Chart(ctx, config);

function resetZoomChart(){
    myChart.resetZoom();
};

function randomColor() {
    var letters = '0123456789abcdef';
    var color = '#';
    for (var i = 0; i <6; i++){
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function meanArrayOfArray(arrayOfArray) {
    //return an array of means values in sub-arrays contained in the array given in parameter 
    var result = [];
    for(i in arrayOfArray){
        var iMean = 0;
        for (j in i){
            iMean += j;
        }
        iMean /= i.lenght;
        result = result.concat(iMean);
    }
    return result;
};

fetch("results.JSON")// fetch and process benchmarking results 
    .then(response => response.json())
    .then(data => {
        for (let i in data["commits"]) {
            //retrieve benchmarked commits hash and use them as label 
            myChart.data.labels.push(data["commits"][i]);
        }
        for (let i in data["benchmark"]) {
            //retrieve metrics name and values 
            for(let j in data["benchmark"][i]){
                for(let k in data["benchmark"][i][j])
                {
                    var color = randomColor();
                    var data_to_put;
                    if(Array.isArray(data["benchmark"][i][j][k][0])){
                        //if current metrics is an array of arrays of values compute an array of
                        //mean of values in sub arrays
                        data_to_put =  meanArrayOfArray(data["benchmark"][i][j][k]);
                    }
                    else{
                        data_to_put = data["benchmark"][i][j][k];
                    };
                    var crafted_dataset = {//create a new dataset from currently processed metric
                        data: data_to_put,
                        label: "\"guix "+ i + "\" " + j + "-" + k,
                        borderColor: color,
                        backgroundColor: color,
                        fill: false,
                    };
                    myChart.data.datasets.push(crafted_dataset);
                }
            }
        }
        myChart.update();
    });
