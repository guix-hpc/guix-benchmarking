## How to use this tool:
Run 'guile script.scm' then 'guile my-web-server.scm' in a terminal.
A base example should be accessible in the browser at 'localhost:8080'. 
In the browser you can zoom on part of the chart by selecting the area 
you want to zoom on, click on dataset legends on top of the chart to 
keep them from displaying on the chart. Clicking on a node of the chart 
will result by a redirection in a new tab to associated commit page in 
the official Guix git repository.

## How to tune 'script.scm':
You can :
    -modify the commit list in 'commit-list' to benchmark the commits you want
    -add new benchmarking procedures
    -modify Guix commands list in 'guix-commands'

## Roadmap:
    Add new benchmarking procedures (disk consumption, disk usage, CPU usage).
    Upgrade old benchmarking procedures so they are run in a isolated child process.
    Upgrade some benchmarking procedures so they can run multiple times.
    Upgrade the JavaScript code so we can make better data visualization.
    Make a better comments and documentation.

For any question or inquiries <hugo.lecomte@inria.fr> 
